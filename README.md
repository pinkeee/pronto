# Pronto

Pronto is a cli based tool to help improve your Hiragana & Katakana skills! With features such as;
 - Accuracy tracking
 - Tips if you are stuck
 - Motivational sentences to keep you learning!