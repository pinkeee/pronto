#pragma once
#include <iostream>

class Data
{
    public:
        std::string name; // we grab this from the config file

        float totalAcc; // we also grab this from the config file (stored in ~.config/)
        
        bool accuracy(bool); // the bool is either true if got answer correct or false if wrong. THIS IS FOR WRITTING ACC

        void getInfo(void); // grabs name and total acc from file. THIS IS FOR READING

        Data(void); // constructor
};