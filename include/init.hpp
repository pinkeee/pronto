#pragma once
#include <map>
#include <sstream>
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <random>

class Setup
{
	public:
		// set up everything
		void readyFiles(void);

		// constructor 
		Setup(void);

	private:
		// this ordered map will contain the romanji as the key and hiragana char as the val
		// it gets inserted in the function "fillMap"
		std::map<std::string, std::string> hiragana;
		std::map<std::string, std::string> katakana;

		// vector to store keys of the map, this makes my life easy when picking a random key
		std::vector<std::string> hiraganaVec;
		std::vector<std::string> katakanaVec;

		// dir for .txt files
		std::string hiraganaDir = "include/hiraganaCharacters.txt";
		std::string katakanaDir = "include/katakanaCharacters.txt";

		// helper func for main process. We pass either 1 for hiragana or 0 for katakana
		void randomCharacter(int);

		std::string randomHira(void); // helper func for randomCharacter
		std::string randomKata(void); // helper func for randomCharacter

		bool init(std::string, std::map<std::string, std::string>&, std::vector<std::string>&); // loads buffers (this looks cleaner than writting two while loops)
};

