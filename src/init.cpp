#include "../include/init.hpp"

// we pass 1 for a hiragana character or 0 for a katakana character
// we are going to use this for a function i havent written yet
inline void Setup::randomCharacter(int x)
{
	switch ((int)x)
	{
		// hiragana
		case 1:
			// call helper func
			this->randomHira();
			break;
		// katakana
		case 0:
			// call helper func
			this->randomKata();
			break;
		default:
			throw;	
	}
}

// select random hiragana character from hiragana map
inline std::string Setup::randomHira(void)
{	
	int randnum = rand() % hiraganaVec.size(); // pick random number between 0 and total size of all the hiragana vector

	// we added all the romanji to this vector too so we can choose a random index of it and return a random romanji
	// where we can then input this into the map and find the corrosponding character. Pretty neat right? (my 3am brain thinks so)
	return this->hiraganaVec[randnum];
}

// select random katakana character from katakana map
inline std::string Setup::randomKata(void)
{	
	int randnum = rand() % katakanaVec.size(); // pick random number between 0 and total size of all the hiragana vector

	// we added all the romanji to this vector too so we can choose a random index of it and return a random romanji
	// where we can then input this into the map and find the corrosponding character. Pretty neat right? (my 3am brain thinks so)
	return this->katakanaVec[randnum];
}

bool Setup::init(std::string loc, std::map<std::string, std::string>& map, std::vector<std::string>& vec)
{	
	std::string line;
	std::ifstream file;

	try {
		file.open(loc, std::fstream::in);
	} catch (std::ios_base::failure& e) {
		std::cout << e.what() << std::endl;
		return false;
	}
	
	while (std::getline(file, line))
	{
		std::istringstream iss(line);
		std::string key, val; // key is romanji and val is character

		if (!(iss >> key >> val)) { std::cout << "error while loading into key and val" << std::endl; return false; } // simple error checking

		map[key] = (std::string)val;
		vec.push_back(key);
	}
	
	return true;
}

void Setup::readyFiles(void)
{	
	if(!this->init(this->hiraganaDir, this->hiragana, this->hiraganaVec)) { std::cout << "error with hiragana load func." << std::endl; return; } // load hiragana map and vector))
	if(!this->init(this->katakanaDir, this->katakana, this->katakanaVec)) { std::cout << "error with katakana load func." << std::endl; return; } // load katakana map and vector))

	auto ranhirara = this->randomHira();
	auto rankataka = this->randomKata();

	std::cout << ranhirara << " : " << this->hiragana[ranhirara] << std::endl;
	std::cout << rankataka << " : " << this->katakana[rankataka] << std::endl;
}

// constructor
Setup::Setup(void)
{
	srand(static_cast<unsigned int>(time(0))); // seed random num gen, we only need to do this once.
	
	this->readyFiles();
	std::cout << "[+] READY" << std::endl;
}
