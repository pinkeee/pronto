TARGET = a.out
#linker
CXX=clang++
# debug
DEBUG=#-v
# optimisation
OPT=-O2
# warnings
WARN=#-Wall -Wextra -Werror -Wpedantic

PTHREAD=-pthread

CCFLAGS=$(DEBUG) $(OPT) $(WARN) $(PTHREAD)

# linker
LDFLAGS=$(PTHREAD) -std=c++17

all: src/*.* include/*.*
	$(CXX) src/*.cpp include/*.hpp $(CCFLAGS) $(LDFLAGS)

clean:
	rm -f *.o src/*.0 src/*.hpp.gch include/*.hpp.gch $(TARGET)
